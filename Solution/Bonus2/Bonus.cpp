#include <iostream>
#include <fstream>

#define FILE_NAME "log.txt"

void redirect_cout(std::ofstream& file);
void reset_cout(std::streambuf* ogCout);

int main() {
	int num = -1;
	bool redirected = false;

	std::ofstream file(FILE_NAME);
	std::streambuf* ogCout = std::cout.rdbuf();

	if (file.is_open()) {

		std::cout << "Enter number to write to screen, 0 to quit, 1 to redirect cout to log.txt and back\n";
		// repeat
		while (num != 0) {
			std::cin >> num;
			
			// redirect
			if (num == 1) {
				if (redirected) {
					reset_cout(ogCout);
				}
				else {
					redirect_cout(file);
				}
				redirected = !redirected;
			}

			std::cout << "You Entered " << num << std::endl;
		}

		reset_cout(ogCout);
		std::cout << "Goodbye!";

		file.close();
	}
	else {
		std::cerr << "Failed to open file\n";
	}
}

void redirect_cout(std::ofstream& file) {
	std::cout.rdbuf(file.rdbuf());
}

void reset_cout(std::streambuf* ogCout) {
	std::cout.rdbuf(ogCout);
}