#include "CaesarText.h"

// constructors
CaesarText::CaesarText(string text) : ShiftText(text, SHIFT_AMOUNT) {
	this->className = "Caesar";
}
CaesarText::CaesarText(string text, bool action) : ShiftText(text, SHIFT_AMOUNT, action)
{
	this->className = "Caesar";
}
