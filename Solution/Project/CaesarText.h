#pragma once
#include "ShiftText.h"

#define SHIFT_AMOUNT 3

class CaesarText : public ShiftText
{
public:
	CaesarText(string text);
	CaesarText(string text, bool action);
	// not sure about this ones... will be sorted out in future commits (hopefully)
	// ~CaesarText
	// string encrypt();
	// string decrypt();
};

