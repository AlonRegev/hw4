#include "FileHelper.h"

// function returns content of file as string
string FileHelper::readFileToString(string fileName)
{
    string buffer = "";
    string str = "";
    std::ifstream file(fileName);
    if (file.is_open()) {
        // go over file line by line
        while (getline(file, buffer)) {
            str += buffer + "\n";
        }

        file.close();
    }
    else {
        std::cerr << "\nFailed to open file.\n";
    }

    return str;
}

// function reads content of file and transfers it to another word by word
void FileHelper::writeWordsToFile(string inputFileName, string outputFileName)
{
    string buffer = "";
    std::ifstream ifile(inputFileName);
    std::ofstream ofile(outputFileName);

    if (ifile.is_open() && ofile.is_open()) {
        // go over file word by word
        while (ifile >> buffer) {
            ofile << buffer << "\n";
        }

        ifile.close();
        ofile.close();
    }
    else {
        std::cerr << "\nFailed to open file.\n";
    }
}
