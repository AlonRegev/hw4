#include "PlainText.h"

int PlainText::_NumOfTexts = 0;

// constructor
PlainText::PlainText(string text)
{
	this->text = text;
	this->isEncrypted = false;

	this->_NumOfTexts++;

	this->className = "Plain Text";
}

// getters
bool PlainText::isEnc() const
{
	return this->isEncrypted;
}
string PlainText::getText() const
{
	return this->text;
}

// operator<< overloading
std::ostream& operator<<(std::ostream& os, const PlainText& t)
{
	os << t.className << "\n" << t.getText() << "\n";
	return os;
}
