#pragma once

#include <string>
#include <iostream>

#define ENCRYPT true
#define DECRYPT false

using std::string;

class PlainText
{
protected:
	std::string text;
	bool isEncrypted;

	string className;

public:
	PlainText(string text);
	// ~PlainText();	using default instead...
	bool isEnc() const;
	string getText() const;

	friend std::ostream& operator<<(std::ostream& os, const PlainText& t);

	static int _NumOfTexts;
};

