#include "ShiftText.h"

// constructors
ShiftText::ShiftText(string text, int key) : PlainText(text)
{
	this->_key = key;

	this->encrypt();

	this->className = "Shift";
}
ShiftText::ShiftText(string text, int key, bool action) : PlainText(text)
{
	this->_key = key;

	if (action == ENCRYPT) { 
		this->encrypt();
	}
	else {
		this->decrypt();
	}

	this->className = "Shift";
}

// function shifts letters in the string.
string ShiftText::encrypt()
{
	char letterOffset = 'a';
	int i = 0;
	for (int i = 0; i < this->text.length(); i++) {
		// finding first letter in case of lower or uppercase
		if ('A' <= this->text[i] && this->text[i] <= 'Z') {
			letterOffset = 'A';
		}
		else if('a' <= this->text[i] && this->text[i] <= 'z') {
			letterOffset = 'a';
		}
		else {
			letterOffset = INVALID;
		}

		if (letterOffset != INVALID) {
			this->text[i] -= letterOffset;	// get letter index 0 to 25
			this->text[i] = (this->text[i] + this->_key) % ('z' - 'a' + 1);	// shift
			// out of range
			if (this->text[i] < 0) {
				this->text[i] += 'z' - 'a' + 1;
			}

			this->text[i] += letterOffset;
		}
	}

	this->isEncrypted = true;

	return this->text;
}

// function returns text to before encryption
string ShiftText::decrypt()
{
	this->_key *= -1;
	this->encrypt();
	this->_key *= -1;
	this->isEncrypted = false;
	return this->text;
}

