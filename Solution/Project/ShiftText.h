#pragma once
#include "PlainText.h"

#define INVALID 0

class ShiftText : public PlainText
{
private:
	int _key;
public:
	ShiftText(string text, int key);
	ShiftText(string text, int key, bool action);
	// ~ShiftText();	using default
	string encrypt();
	string decrypt();
};

