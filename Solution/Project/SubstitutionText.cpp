#include "SubstitutionText.h"

// constructors
SubstitutionText::SubstitutionText(string text, string dictionaryFileName) : PlainText(text)
{
	this->_dictionaryFileName = dictionaryFileName;

	this->readCSV();

	this->encrypt();

	this->className = "Subtitution";
}
SubstitutionText::SubstitutionText(string text, string dictionaryFileName, bool action) : PlainText(text)
{
	this->_dictionaryFileName = dictionaryFileName;

	this->readCSV();

	if (action == ENCRYPT) {
		this->encrypt();
	}
	else {
		this->decrypt();
	}

	this->className = "Subtitution";
}

// function encrypts text using substitution encryption
string SubstitutionText::encrypt()
{
	int i = 0;
	// go over all chars
	for (i = 0; i < this->text.length(); i++) {
		if ('a' <= this->text[i] && this->text[i] <= 'z') {
			this->text[i] = this->_encryptionTable[this->text[i] - 'a'];
		}
	}
	this->isEncrypted = true;

	return this->text;
}

// function decrypts text using substitution encryption
string SubstitutionText::decrypt()
{
	int i = 0;
	// go over all chars
	for (i = 0; i < this->text.length(); i++) {
		if ('a' <= this->text[i] && this->text[i] <= 'z') {
			this->text[i] = this->_decryptionTable[this->text[i] - 'a'];
		}
	}
	this->isEncrypted = false;

	return this->text;
}

// function reads csv file and sets encrtprion / decryption tables based on it
void SubstitutionText::readCSV()
{
	char decrypted = 0;
	char encrypted = 0;
	string buffer = "";
	std::ifstream file(this->_dictionaryFileName);

	if (file.is_open()) {
		// read x,y
		while (std::getline(file, buffer, ',')) {
			decrypted = buffer[0];
			std::getline(file, buffer);
			encrypted = buffer[0];

			// set tables encrypted->decrypted and decrypted->encrypted
			this->_encryptionTable[decrypted - 'a'] = encrypted;
			this->_decryptionTable[encrypted - 'a'] = decrypted;
		}

		file.close();
	}
	else {
		std::cerr << "\nFailed to open file!\n";
	}
}
