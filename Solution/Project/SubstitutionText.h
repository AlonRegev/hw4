#pragma once
#include "PlainText.h"
#include <fstream>
#include <iostream>

class SubstitutionText : public PlainText
{
public:
	SubstitutionText(string text, string dictionaryFileName);
	SubstitutionText(string text, string dictionaryFileName, bool action);
	string encrypt();
	string decrypt();
private:
	string _dictionaryFileName;
	
	// indexes are letters 0 to 25, value is result
	char _encryptionTable['z' - 'a' + 1];
	char _decryptionTable['z' - 'a' + 1];

	// helpers
	void readCSV();
};

