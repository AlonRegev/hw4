#include "SubstitutionText.h"
#include "CaesarText.h"
#include "FileHelper.h"

enum options { USE_STR = 1, USE_FILE, COUNT, QUIT };
enum encryptionTypes {SHIFT = 1, CAESAR, SUBSTITUTION};

#define DISPLAY true
#define SAVE_TO_FILE false

void handleInvalidInput();
string processString(string input, bool action);
void useString();
void useFile();

int main() {
	int choice = 0;
	bool action = ENCRYPT;
	string buffer = "";

	// repeatedly serve user
	do {
		std::cout << "OPTIONS:\n" <<
			"1. encrypt / decrypt string\n" <<
			"2. encrypt / decrypt text file\n" <<
			"3. display use count\n" <<
			"4. quit\n" << 
			"Enter option: ";
		std::cin >> choice;

		// act based of choice
		switch (choice) {
		case USE_STR:
			useString();
			break;

		case USE_FILE:
			useFile();
			break;

		case COUNT:
			std::cout << "Used " << PlainText::_NumOfTexts << " Texts so far...\n\n";
			break;

		case QUIT:
			std::cout << "\nGoodbye :)\n";
			break;

		default:
			std::cout << "Invalid Input!\n";
			break;
		}
	} while (choice != QUIT);

	// check memory
	buffer.~basic_string();

	if (_CrtDumpMemoryLeaks())
	{
		std::cout << "\nMemory leaks!\n";
	}
	else
	{
		std::cout << "\nNo leaks\n";
	}

	return 0;
}

// function gets string and encrypts / decrypts it based on user's choice
string processString(string input, bool action) {
	int choice = 0;
	bool invalid = false;
	string result = "";
	// (many) temp variables  -  classes are ptrs so their constructors won't be called
	ShiftText* shift;
	CaesarText* caesar;
	SubstitutionText* substitution;
	int tempInt = 0;
	string tempStr = "";

	// menu
	std::cout << "Choose encryption method:\n" <<
		"1. Shift\n" <<
		"2. Caesar\n" <<
		"3. Substitution\n" <<
		"Your choice: ";
	
	do {
		invalid = false;
		// get choice
		while (!(std::cin >> choice)) {
			handleInvalidInput();
		}
		// action
		switch (choice) {
		case SHIFT:
			std::cout << "Enter key: ";
			while (!(std::cin >> tempInt)) {
				handleInvalidInput();
			}
			
			shift = new ShiftText(input, tempInt, action);
			result = shift->getText();
			delete shift;
			break;

		case CAESAR:
			caesar = new CaesarText(input, action);
			result = caesar->getText();
			delete caesar;
			break;

		case SUBSTITUTION:
			std::cout << "Enter dictionary file name (dictionary.csv already exists): ";
			std::cin >> tempStr;

			substitution = new SubstitutionText(input, tempStr, action);
			result = substitution->getText();
			delete substitution;
			break;
		default:
			std::cout << "Invalid option!\nEnter again: ";
			invalid = true;
			break;
		}

	} while (invalid);

	return result;
}

// function encrypts / decrypts input from user based on his choices
void useString() {
	string buffer = "";
	bool action = false;

	std::cout << "Enter text: ";
	std::cin >> buffer;
	std::cout << "Do you want to encrypt (1) or decrypt (0)? ";
	while (!(std::cin >> action)) {
		handleInvalidInput();
	}

	buffer = processString(buffer, action);

	std::cout << buffer << "\n\n";
}

// function encrypts / decrypts file from user based on his choices
void useFile() {
	string buffer = "";
	string outFileName = "";
	std::ofstream outFile;
	bool action = false;

	std::cout << "Enter text file name (example.txt exists already): ";
	std::cin >> buffer;
	buffer = FileHelper::readFileToString(buffer);

	std::cout << "Do you want to encrypt (1) or decrypt (0)? ";
	while (!(std::cin >> action)) {
		handleInvalidInput();
	}

	buffer = processString(buffer, action);

	std::cout << "Do you want to display (1) or save result file (0)? ";
	while (!(std::cin >> action)) {
		handleInvalidInput();
	}

	if (action == DISPLAY) {
		std::cout << buffer << "\n\n";
	}
	else {
		std::cout << "Enter output file name: ";
		std::cin >> outFileName;

		outFile.open(outFileName);
		outFile << buffer;
		outFile.close();
	}
}

// function to prevent problems when input is invalid (like wrong type)
void handleInvalidInput() {
	std::cout << "Invalid Input!\n";

	// reset input stream
	std::cin.clear();
	std::cin.ignore(INT_MAX, '\n');
}