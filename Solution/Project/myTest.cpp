#include <iostream>
#include "SubstitutionText.h"
#include "CaesarText.h";

void test() {
	PlainText t1("hello! i'm not encrypted!");
	ShiftText t2("i am... I am shifted by seventeen :(", 17);
	CaesarText t3("i am too, but by three.");
	SubstitutionText t4("subtitution encryption :)", "dictionary.csv");

	std::cout << t1 << t2 << t3 << t4;
}

int tempmain2() {
	test();

	if (_CrtDumpMemoryLeaks())
	{
		std::cout << "\nMemory leaks!\n";
	}
	else
	{
		std::cout << "\nNo leaks\n";
	}

	return 0;
}